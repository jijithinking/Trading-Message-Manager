# 沪深交易所 FIX Binary 数据解析工具

## 介绍

[点此下载本工具](https://gitee.com/CriticalHit/Trading-Message-Manager/releases)

[点此提需求、建议、反馈Bug](https://gitee.com/CriticalHit/Trading-Message-Manager/issues)

联系本人微信：Foveros

工具缘起FIX解析，后续开发了Binary解析、模拟撮合回报等功能。
接下来将支持解析中登所有的日终清算数据（包括日间的RTGS交收数据），文件格式包括但不限于：txt、tsv、dbf、xml

## 软件截图

![数据源选择](https://images.gitee.com/uploads/images/2021/0820/150841_6896210a_8026308.png "数据源选择页.png")
![FIX消息解析](https://images.gitee.com/uploads/images/2021/0820/155409_6adbcb28_8026308.png "FIX消息解析.png")
![Binary消息解析](https://images.gitee.com/uploads/images/2021/0820/155434_aecae366_8026308.png "Binary消息解析.png")
![DBF文件读取](https://images.gitee.com/uploads/images/2021/0820/155453_7683a3be_8026308.png "DBF文件读取.png")

## 安装教程

无需安装，解压后双击运行即可使用。工具还在持续开发中，基本功能已经可以流畅使用，不明白的地方以及需求和建议欢迎加我微信交流：Foveros

## 功能介绍

### 1. 报文解析

支持FIX（包括深交所、上交所的STEP协议，是基于FIX扩展而成的数据交换协议）、Binary两种协议的报文解析

#### 1.1 高扩展性的数据关系结构

本工具设计的初衷是服务全球的交易协议的相关开发人员，提升其开发、测试效率，能将精力更多地专注于业务逻辑、业务系统、系统实施等更需要精力的地方。

以深交所、上交所为例子，交易所本身内部有不同的交易平台（竞价交易平台、固定收益交易平台等），而两个交易所之间又相当于不同的两个平台，如何保证正确解析各个平台的报文，适应不同平台对域的定义以及使用场景上的差别呢？这就是通过高扩展性的数据关系结构设计来保证的，报文中的域，与平台本身，在报文解析的功能实现中，是多对多的关系，这样就保证了平台与平台之间对同一个域的定义不同，能够按照每个平台自身对域的定义来进行解析，保证了解析功能上的高扩展性。

#### 1.2 丰富的数据来源支持

目前支持以下三种数据源作为报文数据来源：

1） **文本**
	a)    **FIX**：

```
8=FIXT.1.19=6935=049=F000621Y001356=127.0.0.1_722034=452=20210418-07:46:02.78610=161
```

​	b)    **Binary**：
​	      i.      **十六进制文本格式**：

```
000000d100000000000000020000000400020002000000db
```

​	      ii.      **Base64加密格式**：

```
AAAAOwAAAAAAAAAIAAAAeDg4MDEzICAgAAADIQAAAAAAAAADAAAAATg4MDEzICAgYTgwMDAwMDA1MjAxMDExMyAgICAgIGE4MDAwMDAwNTEwMDAwMCAgIAAAKrQBNGO+AAABTCPHNMA0MDAxMiAgICAgICAgICAgICAgICAgICAgICAgICAgIAAAANY=
```

2） **日志文件**：发行版的Zip压缩包中附带日志文件样例

3） **SQLSERVER数据库连接**：支持自定义配置实例、数据库、表、字段、Where条件、排序条件



> 文件及数据库支持预先解析每条记录的消息类型，并随记录展示该消息记录的消息类型。
>
> 考虑到日志文件、数据库等数据来源可能存在大量的无用数据（例如心跳），工具提供了**过滤器（Filter）**功能，可使用过滤器指定想要查看（In）的消息类型，也可指定过滤不想要查看（Not in）的消息类型。
>
> 为了更好地定位消息，对于文件及数据库这两种数据来源，支持**数据全文搜索定位**。
>
> 后续会支持文件分内存读取，以支持大体积文件的读取。

####  1.3 一切皆可自定义

在软件本身事先提供了部分FIX消息域、Binary消息域的同时，用户可以在工具中任意地新增消息域。同时也可对已存在的消息域进行域名、域描述等域信息的修改，这个功能适用于交易所等机构在开发了新业务时，新增了自定义域，用户即可在本工具同步新增消息域，以此能够对最新的交易消息格式进行解析。

Binary由于其消息格式的特殊性，是需要用户配置消息内容格式后，才能按照配置的消息内容格式来正常解析Binary消息。

不仅仅是消息域，在本工具中看到的大部分内容，您皆可找到自定义配置的入口。不清楚的地方可以咨询本工具开发者或已经对本工具有使用经验的小伙伴。

#### 1.4 字典支持

一个域，由一个域名及域值组成，域名是必然需要解析出来的，那么域值呢？

对于一个域的值，各个交易平台有着对消息域的值不同的枚举，本工具支持自定义多平台的域消息值枚举，并且在报文解析时，根据选择的平台来对域值进行释义。

本工具同事支持单层、多层、单组、多组重复组嵌套的域值识别。

### 2. 报文重组

![图1 心跳消息解析结果](https://images.gitee.com/uploads/images/2021/0721/122428_71ceee4f_8026308.png "心跳消息解析结果.png")

如 图1 所示，这是一个心跳消息的解析结果，**双击**某一个域，可弹出一个域值编辑窗口，如图2所示

![图2 解析结果的域值编辑](https://images.gitee.com/uploads/images/2021/0721/122458_1810fa30_8026308.png "解析结果的域值编辑.png")

修改该域（56， TargetCompID, 发送方代码）值为127.0.0.1_1234后，点击Confirm即可，修改后的结果将即时地显示在解析结果列表中，对域值完成修改后，点击Remake按钮，即可按照修改后的域值来重新组成报文，并将报文显示在Text栏中，见图3。

![图3 报文重组](https://images.gitee.com/uploads/images/2021/0721/122518_9f6bac4c_8026308.png "报文重组.png")

### 3. 模拟撮合回报

用户可配置各种回报场景，回报规则，从而生成各种业务场景的业务回报，甚至可以通过委托报文生成撤单委托报文，只需一次配置，即可灵活使用。目前支持沪深交易所的所有平台及其平台对应业务（例如深交所固定收益平台、上交所流式竞价平台）。


## 操作指引
1. 消息域
1.1 FIX协议之Tag配置
![Tag配置](https://images.gitee.com/uploads/images/2021/0721/122722_6d39cc0e_8026308.png "FIX之Tag配置.png")
FIX Library标签页 – 与Tag同一水平线的“Manage“按钮，点击进入Tag配置窗口：
![Tag配置说明](https://images.gitee.com/uploads/images/2021/0721/122800_96642c51_8026308.png "Tag配置说明.png")

2. Binary协议解析配置

Binary消息由于其按位读的特性，在解析前需要事先配置好消息格式，沪深交易所对其消息格式都有公开的接口文档提供说明：

[点击查看上交所接口文档](http://www.sse.com.cn/services/tradingservice/tradingtech/technical/development/)

[点击查看深交所接口文档](http://www.szse.cn/marketServices/technicalservice/interface/)

![Binary消息类型未找到](https://images.gitee.com/uploads/images/2021/1008/163655_64a755e9_8026308.png "Binary消息类型未找到.png")

如果解析一个Binary消息前，未配置好该Binary消息的消息类型，则会解析失败，并且弹出如图提示，提示用户应当先配置该消息类型的消息格式，再进行解析

2.1 添加消息类型

![添加Binary消息类型](https://images.gitee.com/uploads/images/2021/1008/164144_882f0090_8026308.png "添加Binary消息类型.png")

2.2 为消息类型配置消息格式

![配置Binary消息格式](https://images.gitee.com/uploads/images/2021/1008/164512_2facd3bb_8026308.png "配置Binary消息格式.png")


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request